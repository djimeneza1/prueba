﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;
using Dal;

namespace Bussiness
{
    public class ApiBussiness
    {
        public ApiDal Dal;

        public ApiBussiness(string Conexion)
        {
            Dal = new ApiDal(Conexion);
        }
        //LOGIN
        public List<UserDto> GetLogin(string Usuario, string Password)
        {
            return Dal.GetLogin(Usuario, Password);

        }

        //ACTUALIZAR USUARIO
        public bool InsertUsuarios(UsuariosDto Datos)
        {
            return Dal.InsertUsuarios(Datos);

        }

        //PERMISOS
        public List<PermisosDto> GetPermisos(string Usuario)
        {
            return Dal.GetPermisos(Usuario);

        }

        //INVENTARIO
        public List<InventarioDto> GetInventario( )
        {
            return Dal.GetInventario();

        }

        public bool InsertaInventario(InventarioDto inventario)
        {
            return Dal.InsertaInventario(inventario);

        }


        public bool ActualizaInventario(InventarioDto inventario)
        {
            return Dal.ActualizaInventario(inventario);

        }


        public bool BorraInventario(InventarioDto inventario)
        {
            return Dal.BorraInventario(inventario);

        }


        //VENTAS

        public List<VentasDto> GetVentas()
        {
            return Dal.GetVentas();

        }


        public bool EliminaVentas(VentasDto ventas)
        {
            return Dal.EliminaVentas(ventas);

        }

        public List<InventarioDto> GetProductos()
        {
            return Dal.GetProductos();

        }

        public bool InsertaVentas(VentasDto ventas)
        {
            return Dal.InsertaVentas(ventas);

        }
    }
}

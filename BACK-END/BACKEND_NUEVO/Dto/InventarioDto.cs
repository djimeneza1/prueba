﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class InventarioDto
    {
        public int Id { get; set; }
        public string Lote {get; set; }
      public string Nombre {get; set; }
      public string Precio {get; set; }
      public string Cantidad {get; set; }
      public string Ingreso {get; set; }
    }
}

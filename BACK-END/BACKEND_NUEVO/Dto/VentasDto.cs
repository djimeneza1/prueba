﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class VentasDto
    {
       public int Id {get; set; }
      public string FechaCompra { get; set; }
      public string Cliente { get; set; }
      public string Productos { get; set; }
      public string CantidadProductos { get; set; }
      public string PrecioTotal { get; set; }
    }
}

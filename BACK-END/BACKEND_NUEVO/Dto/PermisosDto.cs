﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class PermisosDto
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Usuario { get; set; }
        public int IdRol { get; set; }
        public int IdPermiso { get; set; }
        public string Permiso { get; set; }
    }
}

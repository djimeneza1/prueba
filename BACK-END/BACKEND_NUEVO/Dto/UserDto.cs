﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string Identificacion { get; set; }
        public int Rol { get; set; }
        public int IdPermiso { get; set; }
        public string Permiso { get; set; }
    }
}

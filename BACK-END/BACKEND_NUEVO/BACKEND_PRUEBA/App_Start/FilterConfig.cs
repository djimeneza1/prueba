﻿using System.Web;
using System.Web.Mvc;

namespace BACKEND_PRUEBA
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

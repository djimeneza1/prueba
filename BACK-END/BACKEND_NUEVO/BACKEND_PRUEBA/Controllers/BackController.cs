﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bussiness;
using Dto;

namespace BACKEND_PRUEBA.Controllers
{
    public class BackController : ApiController
    {
        public ApiBussiness Bussines = new ApiBussiness(Properties.Settings.Default.Conexion);

        [HttpGet, Route("GetLogin")]
        public List<UserDto> GetLogin(string Usuario, string Password)
        {
            return Bussines.GetLogin(Usuario, Password);
        }


        //Metodo para Crear usuario
        [HttpPost, Route("InsertUsuarios")]
        public bool InsertUsuarios([FromBody] UsuariosDto Datos)
        {
            return Bussines.InsertUsuarios(Datos);
        }

        //Metodo para TENER PERMISOS
        [HttpGet, Route("GetPermisos")]
        public List<PermisosDto> GetPermisos(string Usuario)
        {
            return Bussines.GetPermisos(Usuario);
        }


        //Metodo para TENER INVENTARIO

        [HttpGet, Route("GetInventario")]
        public List<InventarioDto> GetInventario()
        {
            return Bussines.GetInventario();
        }


        //Metodo para Crear Producto

        [HttpPost, Route("InsertaInventario")]
        public bool InsertaInventario([FromBody] InventarioDto inventario)
        {
            return Bussines.InsertaInventario(inventario);
        }


        [HttpPost, Route("ActualizaInventario")]
        public bool ActualizaInventario([FromBody] InventarioDto inventario)
        {
            return Bussines.ActualizaInventario(inventario);
        }

        [HttpPost, Route("BorraInventario")]
        public bool BorraInventario([FromBody] InventarioDto inventario)
        {
            return Bussines.BorraInventario(inventario);
        }

        //VENTAS


        [HttpGet, Route("GetVentas")]
        public List<VentasDto> GetVentas()
        {
            return Bussines.GetVentas();
        }

        [HttpPost, Route("EliminaVentas")]
        public bool EliminaVentas([FromBody] VentasDto ventas)
        {
            return Bussines.EliminaVentas(ventas);
        }

        [HttpGet, Route("GetProductos")]
        public List<InventarioDto> GetProductos()
        {
            return Bussines.GetProductos();
        }


        [HttpPost, Route("InsertaVentas")]
        public bool InsertaVentas([FromBody] VentasDto ventas)
        {
            return Bussines.InsertaVentas(ventas);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;

namespace Dal
{
    public class ApiDal : Dapper
    {
        public ApiDal(string ConexionString)
        {
            Conexion = ConexionString;
        }

        //SP QUE TRAE INFORMACIÓN DE USUARIO Y CONTRASEÑA 
        public List<UserDto> GetLogin(string Usuario, string Password)
        {
            return ListQuery<object, UserDto>("dbo.SP_GetLogin", new { Usuario = Usuario, Password = Password });
        }

        //SP QUE CREA USUARIO
        public bool InsertUsuarios(UsuariosDto Datos)
        {
            return SingleQuery<UsuariosDto, bool>("dbo.SP_CrearUsuario", Datos);
        }



        //SP QUE TRAE INFORMACIÓN DE PERMISOS POR USUARIO
        public List<PermisosDto> GetPermisos(string Usuario)
        {
            return ListQuery<object, PermisosDto>("dbo.SP_GetPermisos", new { Usuario = Usuario });
        }

        //SP QUE TRAE INFORMACIÓN DE INVENTARIO
        public List<InventarioDto> GetInventario()
        {
            return ListQuery<object, InventarioDto>("dbo.SP_GetInventario", new {});
        }


        //SP QUE CREA PRODUCTO
        public bool InsertaInventario(InventarioDto inventario)
        {
            return SingleQuery<InventarioDto, bool>("dbo.Sp_InsertaInventario", inventario);
        }

        //SP QUE ACTUALIZA PRODUCTO
        public bool ActualizaInventario(InventarioDto inventario)
        {
            return SingleQuery<InventarioDto, bool>("dbo.Sp_ActualizaInventario", inventario);
        }

        public bool BorraInventario(InventarioDto inventario)
        {
            return SingleQuery<InventarioDto, bool>("dbo.Sp_BorraInventario", inventario);
        }

        //VENTAS
        public List<VentasDto> GetVentas()
        {
            return ListQuery<object, VentasDto>("dbo.SP_GetVentas", new { });
        }

        public bool EliminaVentas(VentasDto ventas)
        {
            return SingleQuery<VentasDto, bool>("dbo.Sp_EliminaVentas", ventas);
        }


        public List<InventarioDto> GetProductos()
        {
            return ListQuery<object, InventarioDto>("dbo.SP_GetProductos", new { });
        }

        public bool InsertaVentas(VentasDto ventas)
        {
            return SingleQuery<VentasDto, bool>("dbo.Sp_InsertaVentas", ventas);
        }


    }
}

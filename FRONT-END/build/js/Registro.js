


form.addEventListener('submit', e => {
    e.preventDefault();
    traerCampos();

});




function traerCampos() {
    var entradaUsuario = $('#username').val();
    var entradaPassword = btoa($('#pass').val());
    var Nombre = $('#Nombre').val();
    var Apellido = $('#Apellido').val();
    var Identificacion = $('#Identificacion').val();
    var IdRol = 1
    if (entradaUsuario == "" || entradaPassword == "" || Nombre == "" || Apellido == "" || Identificacion == "") {
        swal.fire("Ups!", "Todos los campos son obligatorios", "error")
        return false;
    }


    var Asesor = {
        Usuario: entradaUsuario,
        Password: entradaPassword,
        Identificacion: Identificacion,
        Nombre: Nombre,
        Apellido: Apellido,
        IdRol: IdRol,

    };

    console.log(Asesor)

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: 'https://localhost:44379/InsertUsuarios',
        data: JSON.stringify(Asesor),

        beforeSend: function () { },
        success: function (response) {
            console.log(response)


            if (response == false) {
                swal.fire("Bien!", "El Usuario fue guardado con éxito", "success").then((result) => {
                    if (result.isConfirmed) {

                        window.location.href = 'index.html'
                        console.log(requestOptions)
                    }
                });
            } else {
                swal.fire("Upps!", "No pudo guardarse el registro", "error");
            }

        },
        error: function (err) { },
        timeout: 300000

    });



}
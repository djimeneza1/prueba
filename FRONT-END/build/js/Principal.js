var idEditar;
var idEliminar;

(function () {
    validaUsuario();
    usuarioLocalStorage = localStorage.getItem('userCalc');
    usuarioLocalStorage = atob(usuarioLocalStorage);
    usuario = JSON.parse(usuarioLocalStorage);
    //console.log(usuario[0].Rol);
    console.log(usuario[0].Usuario);
    var user = usuario[0].Usuario;
    $.ajax({
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: "https://localhost:44379/GetPermisos?Usuario=" + user,
        success: function (response) {

            console.log(response)


            //DASHBOARD
            var Inventario = response.filter(function (el) {
                return (el.Permiso == "Inventario");

            });

            if (Inventario == "[]" || Inventario == "") {
                console.log("no tiene permisos de Inventario")
                document.getElementById('main').style.display = 'none';
                window.location.assign("index.html")
                
            } else {
                console.log(Inventario)
            }

            //REPORTES

            var Compra = response.filter(function (el) {
                return (el.Permiso == "Compra");

            });

            if (Compra == "[]" || Compra == "") {
                console.log("no tiene permisos de Compra")
                document.getElementById('Compra').style.display = 'none';
            } else {
                console.log(Compra)
            }







        },
        error: function (err) { },
        timeout: 300000

    });
})();

function validaUsuario() {
    usuarioLocalStorage = localStorage.getItem('userCalc');
    if (usuarioLocalStorage != null && usuarioLocalStorage != '' && usuarioLocalStorage != '[]' && usuarioLocalStorage != '0') {
        usuarioLocalStorage = atob(usuarioLocalStorage);
        usuario = JSON.parse(usuarioLocalStorage);
        var usuarioString = JSON.stringify(usuario);
        if (usuario != null) {
            //ValidaRol();
        }
    } else {
        window.location.assign("Index.html");
    }
}



$(document).ready(function () {

    TraerDatos();
    TraerDatosVentas();

});

function TraerDatos() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: "https://localhost:44379/GetInventario",
        success: function (response) {
            $("#ContenedorInventario").empty();
            numeroAleatorio = Math.floor(Math.random());
            $("#ContenedorInventario").append("<table style='cursor:pointer;' id='MyDynamicTable" + numeroAleatorio +
                "' class='display table table-bordred table-striped'><thead id='theadTablaPrincipal'>" +
                "<tr><th>Número Lote</th><th>Nombre</th><th>Precio</th><th>Cantidad</th><th>Ingreso</th><th>Editar</th><th>Eliminar</th>" +
                "</tr></thead><tfoot><tr><th>Número Lote</th><th>Nombre</th><th>Precio</th><th>Cantidad</th><th>Ingreso</th><th>Editar</th><th>Eliminar</th>" +
                "</tr></tfoot></table>");

            $("#ContenedorInventario").append("<br></br>");
            var spanish = {
                "processing": "Procesando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "infoThousands": ",",
                "loadingRecords": "Cargando...",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            };
            var dataTableInstance = $("#MyDynamicTable" + numeroAleatorio).DataTable({
                data: response,
                "columns": [
                {
                    "data": "Lote"
                },
                {
                    "data": "Nombre"
                },
                {
                    "data": "Precio"
                },
                {
                    "data": "Cantidad"
                },
                {
                    "data": "Ingreso"
                },
                {
                    "data": "Ingreso"
                },
                {
                    "data": "Ingreso"
                },
                ],
                "language": spanish
            });
            $("#MyDynamicTable" + numeroAleatorio + " thead th").each(function (index, value) {
                if (index == 0) {
                    var title = $("#MyDynamicTable" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                } else if (index == 1) {
                    var title = $("#MyDynamicTable" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                } else if (index == 3) {
                    var title = $("#MyDynamicTable" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                } else if (index == 5) {
                    var title = $("#MyDynamicTable" + numeroAleatorio + " tfoot th").eq($(this).index()).text();

                } else if (index == 6) {
                    var title = $("#MyDynamicTable" + numeroAleatorio + " tfoot th").eq($(this).index()).text();

                } 
                else {
                    var title = $("#MyDynamicTable" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                }

            });
            $("tfoot").hide();
            $("#MyDynamicTable" + numeroAleatorio + "_filter").hide();
            dataTableInstance.columns().every(function () {
                var dataTableColumn = this;
                var searchTextBoxes = $(this.header()).find('input');
                var selectBoxes = $(this.header()).find('select');
                searchTextBoxes.on('keyup change', function () {
                    dataTableColumn.search(this.value).draw();
                });
                searchTextBoxes.on('click', function (e) {
                    e.stopPropagation();
                });
                selectBoxes.on('keyup change', function () {
                    dataTableColumn.search(this.value).draw();
                });
                selectBoxes.on('click', function (e) {
                    e.stopPropagation();
                });
            });
            $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                $("td").each(function () {
                    $(this).css('background', '#FFFFFF');
                });

                $(this).find('td').css('background', '#C3EAFD');

                 ultimoElementoSeleccionado = dataTableInstance.row(this).data();

                 idEditar = ultimoElementoSeleccionado.Id;

                 var NumeroLote = ultimoElementoSeleccionado.Lote;
                 var Nombre = ultimoElementoSeleccionado.Nombre;
                 var Precio = ultimoElementoSeleccionado.Precio;
                 var Cantidad = ultimoElementoSeleccionado.Cantidad;
                 var Ingreso = ultimoElementoSeleccionado.Ingreso;
                 
                 console.log(idEditar)

                 $("#campos").empty();
                 $("#campos").append("<div class='col-md-4 mb-3'><label for='LoteActu'>Número de lote</label><input type='text' class='form-control' id='LoteActu' value="+NumeroLote+" required/><div class='invalid-feedback'>El Campo no puede estar vacio.</div></div>");

                 $("#campos").append("<div class='col-md-4 mb-3'><label for='NombreActu'>Nombre</label><input type='text' class='form-control' id='NombreActu' value="+Nombre+" required/><div class='invalid-feedback'>El Campo no puede estar vacio.</div></div>");


                 $("#campos").append("<div class='col-md-4 mb-3'><label for='PrecioActu'>Precio</label><input type='text' class='form-control' id='PrecioActu' value="+Precio+" required/><div class='invalid-feedback'>El Campo no puede estar vacio.</div></div>");


                 $("#campos").append("<div class='col-md-4 mb-3'><label for='CantidadActu'>Cantidad</label><input type='text' class='form-control' id='CantidadActu' value="+Cantidad+" required/><div class='invalid-feedback'>El Campo no puede estar vacio.</div></div>");


                 $("#campos").append("<div class='col-md-4 mb-3'><label for='IngresoActu'>Fecha Ingreso</label><input type='date' class='form-control' id='IngresoActu' value="+Ingreso+" required/><div class='invalid-feedback'>El Campo no puede estar vacio.</div></div>");




                $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                    filter: 'applied'
                }).nodes();

            });
            contador = 0;
            $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                if (value._DT_CellIndex != undefined) {
                    if (value._DT_CellIndex.column == 5) {
                        var textoCampo = $(this).text();
                        $(this).empty();
                        $(this).append("<img src='http://127.0.0.1:5500/build/images/Editar.png' style='width:20px;' onclick='EditarProducto()'/>");
                        contador++;
                    }
                    if (value._DT_CellIndex.column == 6) {
                        var textoCampo = $(this).text();
                        $(this).empty();
                        $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                        contador++;
                    }
                }
            });
            $("#MyDynamicTable" + numeroAleatorio + "_paginate").on('click', function () {
                contador = 0;
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".buscadores").keyup(function (e) {
                contador = 0;
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".buscadores").on('change', function () {
                contador = 0;
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }

                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".sorting").on('click', function () {
                contador = 0;
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".sorting_desc").on('click', function () {
                contador = 0;
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".sorting_asc").on('click', function () {
                contador = 0;
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $("#MyDynamicTable" + numeroAleatorio + "_paginate").on('click', function () {
                contador = 0;
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $("#MyDynamicTable" + numeroAleatorio + " thead .sorting").on('click', function () {
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();

                });
            });
            $("#MyDynamicTable" + numeroAleatorio + " thead .sorting_desc").on('click', function () {
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {

                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $("#MyDynamicTable" + numeroAleatorio + " thead .sorting_asc").on('click', function () {
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src=" +BackendImage + "Editar.png style='width:20px;' onclick='EditarProducto()'/>");
                            contador++;
                        } if (value._DT_CellIndex.column == 6) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarProducto()'/>");
                            contador++;
                        }
                    }
                });
                $("#MyDynamicTable" + numeroAleatorio + " tbody tr").on('click', function () {
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });

                    $(this).find('td').css('background', '#C3EAFD');

                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();


                    $("#MyDynamicTable" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
        },
        error: function (err) { },
        timeout: 300000

    });
}

function TraerDatosVentas() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: "https://localhost:44379/GetVentas",
        success: function (response) {
            $("#ContenedorVentas").empty();
            numeroAleatorio = Math.floor(Math.random());
            $("#ContenedorVentas").append("<table style='cursor:pointer;' id='MyDynamicTable2" + numeroAleatorio +
                "' class='display table table-bordred table-striped'><thead id='theadTablaPrincipal'>" +
                "<tr><th>Fecha Compra</th><th>Cliente</th><th>Productos Comprados</th><th>Cantidad por producto</th><th>Precio Total</th><th>Editar</th>" +
                "</tr></thead><tfoot><tr><th>Fecha Compra</th><th>Cliente</th><th>Productos Comprados</th><th>Cantidad por producto</th><th>Precio Total</th><th>Editar</th>" +
                "</tr></tfoot></table>");
  
            $("#ContenedorVentas").append("<br></br>");
            var spanish = {
                "processing": "Procesando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "infoThousands": ",",
                "loadingRecords": "Cargando...",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            };
            var dataTableInstance = $("#MyDynamicTable2" + numeroAleatorio).DataTable({
                data: response,
                "columns": [
                {
                    "data": "FechaCompra"
                },
                {
                    "data": "Cliente"
                },
                {
                    "data": "Productos"
                },
                {
                    "data": "CantidadProductos"
                },
                {
                    "data": "PrecioTotal"
                },
                {
                    "data": "PrecioTotal"
                },
                ],
                "language": spanish
            });
            $("#MyDynamicTable2" + numeroAleatorio + " thead th").each(function (index, value) {
                if (index == 0) {
                    var title = $("#MyDynamicTable2" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                } else if (index == 1) {
                    var title = $("#MyDynamicTable2" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                } else if (index == 3) {
                    var title = $("#MyDynamicTable2" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                } else if (index == 5) {
                    var title = $("#MyDynamicTable2" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
  
                } else if (index == 6) {
                    var title = $("#MyDynamicTable2" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
  
                } 
                else {
                    var title = $("#MyDynamicTable2" + numeroAleatorio + " tfoot th").eq($(this).index()).text();
                    $(this).html('<h5>' + title + '</h5><input type="text" placeholder="' + title + '" class="buscadores" style="width: 100px;"/>');
                }
  
            });
            $("tfoot").hide();
            $("#MyDynamicTable2" + numeroAleatorio + "_filter").hide();
            dataTableInstance.columns().every(function () {
                var dataTableColumn = this;
                var searchTextBoxes = $(this.header()).find('input');
                var selectBoxes = $(this.header()).find('select');
                searchTextBoxes.on('keyup change', function () {
                    dataTableColumn.search(this.value).draw();
                });
                searchTextBoxes.on('click', function (e) {
                    e.stopPropagation();
                });
                selectBoxes.on('keyup change', function () {
                    dataTableColumn.search(this.value).draw();
                });
                selectBoxes.on('click', function (e) {
                    e.stopPropagation();
                });
            });
            $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                $("td").each(function () {
                    $(this).css('background', '#FFFFFF');
                });
  
                $(this).find('td').css('background', '#C3EAFD');
  
                 ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
                 idEliminar = ultimoElementoSeleccionado.Id;

  
  
                $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                    filter: 'applied'
                }).nodes();
  
            });
            contador = 0;
            $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                if (value._DT_CellIndex != undefined) {
                    if (value._DT_CellIndex.column == 5) {
                        var textoCampo = $(this).text();
                        $(this).empty();
                        $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                        contador++;
                    }

                }
            });
            $("#MyDynamicTable2" + numeroAleatorio + "_paginate").on('click', function () {
                contador = 0;
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".buscadores").keyup(function (e) {
                contador = 0;
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".buscadores").on('change', function () {
                contador = 0;
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
  
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".sorting").on('click', function () {
                contador = 0;
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".sorting_desc").on('click', function () {
                contador = 0;
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $(".sorting_asc").on('click', function () {
                contador = 0;
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $("#MyDynamicTable2" + numeroAleatorio + "_paginate").on('click', function () {
                contador = 0;
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $("#MyDynamicTable2" + numeroAleatorio + " thead .sorting").on('click', function () {
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
  
                });
            });
            $("#MyDynamicTable2" + numeroAleatorio + " thead .sorting_desc").on('click', function () {
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
  
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
            $("#MyDynamicTable2" + numeroAleatorio + " thead .sorting_asc").on('click', function () {
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr td").each(function (index, value) {
                    if (value._DT_CellIndex != undefined) {
                        if (value._DT_CellIndex.column == 5) {
                            var textoCampo = $(this).text();
                            $(this).empty();
                            $(this).append("<img src='http://127.0.0.1:5500/build/images/Eliminar.png' style='width:20px;' onclick='EliminarVenta()'/>");
                            contador++;
                        } 
                    }
                });
                $("#MyDynamicTable2" + numeroAleatorio + " tbody tr").on('click', function () {
                    $("td").each(function () {
                        $(this).css('background', '#FFFFFF');
                    });
  
                    $(this).find('td').css('background', '#C3EAFD');
  
                     ultimoElementoSeleccionado = dataTableInstance.row(this).data();
  
  
                    $("#MyDynamicTable2" + numeroAleatorio).DataTable().rows({
                        filter: 'applied'
                    }).nodes();
                    ultimaVentaSeleccionada = dataTableInstance.row(this).data();
                });
            });
        },
        error: function (err) { },
        timeout: 300000
  
    });
  }
  




function EditarProducto(){
    $("#GSCCModal").modal("show");
}





function ActuProducto(){
    var LoteActu = $("#LoteActu").val();
    var NombreActu = $("#NombreActu").val();
    var PrecioActu = $("#PrecioActu").val();
    var CantidadActu = $("#CantidadActu").val();
    var IngresoActu = $("#IngresoActu").val();

    if (LoteActu == "" || NombreActu == "" || PrecioActu == "" || CantidadActu == "" || IngresoActu == "") {
        swal.fire("Ups!", "Todos los campos son obligatorios", "error")
        return false;
    }


    var Producto = {
        Id: idEditar,
        Lote: LoteActu,
        Nombre: NombreActu,
        Precio: PrecioActu,
        Cantidad: CantidadActu,
        Ingreso: IngresoActu,
    };

    console.log(Producto)

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: 'https://localhost:44379/ActualizaInventario',
        data: JSON.stringify(Producto),

        beforeSend: function () { },
        success: function (response) {
            console.log(response)


            if (response == false) {
                swal.fire("Bien!", "El Producto fue editado con éxito", "success").then((result) => {
                    if (result.isConfirmed) {

                        window.location.href = 'main.html'
                        console.log(requestOptions)
                    }
                });
            } else {
                swal.fire("Upps!", "No pudo guardarse el registro", "error");
            }

        },
        error: function (err) { },
        timeout: 300000

    });
}

function GuardarProducto(){
    var LoteAgre = $("#LoteAgre").val();
    var NombreAgre = $("#NombreAgre").val();
    var PrecioAgre = $("#PrecioAgre").val();
    var CantidadAgre = $("#CantidadAgre").val();
    var IngresoAgre = $("#IngresoAgre").val();

    if (LoteAgre == "" || NombreAgre == "" || PrecioAgre == "" || CantidadAgre == "" || IngresoAgre == "") {
        swal.fire("Ups!", "Todos los campos son obligatorios", "error")
        return false;
    }


    var Producto = {
        Id: 1,
        Lote: LoteAgre,
        Nombre: NombreAgre,
        Precio: PrecioAgre,
        Cantidad: CantidadAgre,
        Ingreso: IngresoAgre,
    };

    console.log(Producto)

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        url: 'https://localhost:44379/InsertaInventario',
        data: JSON.stringify(Producto),

        beforeSend: function () { },
        success: function (response) {
            console.log(response)


            if (response == false) {
                swal.fire("Bien!", "El Producto fue guardado con éxito", "success").then((result) => {
                    if (result.isConfirmed) {

                        window.location.href = 'main.html'
                        console.log(requestOptions)
                    }
                });
            } else {
                swal.fire("Upps!", "No pudo guardarse el registro", "error");
            }

        },
        error: function (err) { },
        timeout: 300000

    });


}

function EliminarProducto(){


    Swal
    .fire({
        title: "¿Está seguro de eliminar el producto?",
        text: idEditar,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: "Sí, Eliminar",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if (resultado.value) {


                    var myHeaders = new Headers();
                    myHeaders.append("Content-Type", "application/json");
                    var raw = JSON.stringify({

                        "Id": idEditar,


                    });

                    var requestOptions = {
                        method: 'POST',
                        headers: myHeaders,
                        body: raw,
                        redirect: 'follow'
                    };
                    fetch("https://localhost:44379/BorraInventario", requestOptions)
                        .then(response => response.text(
                            console.log(response)
                        ))
                        .then(result => {
                            var resultado = result;
                            console.log(result)
                            if (resultado != "false") {
                                swal.fire("Upps!", "No pudo eliminarse el registro", "error");
                            } else if (resultado == "false") {
                                swal.fire("Ey!", "El prodcuto seleccionado se ha eliminado con éxito", "success").then((result) => {
                                    if (result.isConfirmed) {
                                        window.location.href = 'main.html'
                                    }
                                });
                            } else {
                                alert("")
                            }
                        })
                        .catch(error => console.log('error', error));

        

        } else {
            // Dijeron que no
            console.log("*NO se elimina la venta*");
            return false;
        }
    });


}

function EliminarVenta(){

    Swal
    .fire({
        title: "¿Está seguro de eliminar la Venta?",
        text: idEliminar,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: "Sí, Eliminar",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if (resultado.value) {


                    var myHeaders = new Headers();
                    myHeaders.append("Content-Type", "application/json");
                    var raw = JSON.stringify({

                        "Id": idEliminar,


                    });

                    var requestOptions = {
                        method: 'POST',
                        headers: myHeaders,
                        body: raw,
                        redirect: 'follow'
                    };
                    fetch("https://localhost:44379/EliminaVentas", requestOptions)
                        .then(response => response.text(
                            console.log(response)
                        ))
                        .then(result => {
                            var resultado = result;
                            console.log(result)
                            if (resultado != "false") {
                                swal.fire("Upps!", "No pudo eliminarse el registro", "error");
                            } else if (resultado == "false") {
                                swal.fire("Ey!", "La venta seleccionado se ha eliminado con éxito", "success").then((result) => {
                                    if (result.isConfirmed) {
                                        window.location.href = 'main.html'
                                    }
                                });
                            } else {
                                alert("")
                            }
                        })
                        .catch(error => console.log('error', error));

        

        } else {
            // Dijeron que no
            console.log("*NO se elimina la venta*");
            return false;
        }
    });


}


const form = document.getElementById('form');
const usuario2 = document.getElementById('username');
const password = document.getElementById('pass');



form.addEventListener('submit', e => {
	e.preventDefault();
	traerCampos(); 
	checkInputs();
});

function checkInputs() {


	// trim to remove the whitespaces
	const usuarioValue = usuario2.value.trim();

	const passwordValue = password.value.trim();
	
	if(usuarioValue === '' ) {

		setErrorFor(usuario2, 'No puede dejar el usuario en blanco');
	} 

	
	if(passwordValue === '') {

		setErrorFor(password, 'La contraseña no debe ingresar en blanco.');
	} 
	
}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control1 error';
	small.innerText = message;
}

function setErrorFor2(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control1 error';
	small.innerText = "Usuario o contraseñas incorrectos";
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control1 success';
}



function traerCampos() {
	var entradaUsuario = $('#username').val();
	var entradaPassword = btoa($('#pass').val());
	if (entradaUsuario == "" || entradaPassword == "") {
		checkInputs();
		return false;
	}
	
	
	var Asesor = {
		Usuario: entradaUsuario,
		Password: entradaPassword
	};
	
	console.log(Asesor)
	
	
	if (entradaUsuario != "" && entradaPassword != "") {
		$.ajax({
			type: 'GET',
			dataType: 'json',
			contentType: "application/json; charset=utf-8",
			url: "https://localhost:44379/GetLogin?Usuario="+entradaUsuario+"&Password="+entradaPassword,
			success: function(response) {
				if (JSON.stringify(response) == "[]") {
					setErrorFor2(password);
					setErrorFor2(usuario2);
					console.log(response);
				} else {
					setSuccessFor(password);
					setSuccessFor(usuario2);
					var usuario = JSON.stringify(response);
					var usuarioEncriptado = btoa(usuario);
					localStorage.setItem('userCalc', usuarioEncriptado);
					var usuariojs = JSON.parse(usuario);
					console.log(usuariojs[0].Rol);
					console.log(usuario)
					//alert("Si")
					
					window.location.assign('Compras.html')
					
					
					
				}
	
			},
			error: function(err) {},
			timeout: 300000
	
		});
	}
	
	}
	